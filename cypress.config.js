const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    reporter: "cypress-multi-reporters",
    reporterEnabled: "mochawesome, mocha-junit-reporter",
    reporterOptions: {
      configFile: 'report-config.json',
    },
    viewportWidth: 1280,
    viewportHeight: 720,
    trashAssetsBeforeRuns: true,
    video: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});