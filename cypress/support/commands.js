import addContext from 'mochawesome/addContext';

function overwriteRequest() {
    Cypress.Commands.overwrite('request', (originalFn, ...options) => {
        const optionsObject = options[0];
        const token = Cypress.env('token');

        if (!!token && optionsObject === Object(optionsObject)) {
            optionsObject.headers = {
                authorization: token,
                ...optionsObject.headers,
            };

            return originalFn(optionsObject);
        }

        return originalFn(...options);
    });
}

Cypress.Commands.add('getToken', getToken);
Cypress.Commands.add('addContext', (context) => {
    cy.once('test:after:run', (test) => addContext({ test }, context));
});

function getToken(fixture) {
    var result;
    cy.fixture(fixture).then(body => {
        cy.request({
            url: body.url,
            method: 'POST',
            body
        }).then(response => {
            result = {
                body,
                response
            };
            Cypress.env('token', 'Bearer ' + response.body.access_token);
        });
    }).then(() => {
        overwriteRequest();
        expect(result.response.status).to.eq(200);
        return result;
    });
}