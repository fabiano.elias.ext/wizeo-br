describe('mobile', () => {
  const baseUrl = 'https://qa.mobile.api.fuelandfleet.com';
  before('/oauth/token', function () {
    cy.getToken('oauthTokenMobile.json');
  });

  it('POST /api/m/user/card/assign', () => {
    cy.request({
      url: `${baseUrl}/api/m/user/card/assign`,
      method: 'POST',
      failOnStatusCode: false,
      body:{
        card: '6060720725844356',
        pin: '970022'
      }
    }).then(response => {
      expect(response.status).to.eq(403);
    })
  })
})