describe('merchant', () => {
  const baseUrl = 'https://qa.merchant.api.fuelandfleet.com';
  before('/oauth/token', function () {
    cy.getToken('oauthTokenMerchant.json');
  });

  it('GET /api/me/dashboard/graph', () => {
    cy.request({
      url: `${baseUrl}/api/me/dashboard/graph`,
      method: 'GET'
    }).then(response => {
      expect(response.status).to.eq(200);
      cy.addContext(`Response.Body: ${JSON.stringify(response.body)}`);
    })
  })

  it('GET /api/me/dashboard/cities/:uf', () => {
    cy.request({
      url: `${baseUrl}/api/me/dashboard/cities/sp`,
      method: 'GET'
    }).then(response => {
      expect(response.status).to.eq(200);
      cy.addContext(`Response.Body: ${JSON.stringify(response.body)}`);
    })
  })

  it('GET /api/me/dashboard/ufs', () => {
    cy.request({
      url: `${baseUrl}/api/me/dashboard/ufs`,
      method: 'GET'
    }).then(response => {
      expect(response.status).to.eq(200);
      cy.addContext(`Response.Body: ${JSON.stringify(response.body)}`);
    })
  })

  it('GET /api/me/dashboard/user/details', () => {
    cy.request({
      url: `${baseUrl}/api/me/dashboard/user/details`,
      method: 'GET'
    }).then(response => {
      expect(response.status).to.eq(200);
      cy.addContext(`Response.Body: ${JSON.stringify(response.body)}`);
    })
  })

  it('GET /api/me/dashboard', () => {
    cy.request({
      url: `${baseUrl}/api/me/dashboard`,
      method: 'GET'
    }).then(response => {
      expect(response.status).to.eq(200);
      cy.addContext(`Response.Body: ${JSON.stringify(response.body)}`);
    })
  })
})